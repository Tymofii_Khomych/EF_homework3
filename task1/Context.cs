﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Metrics;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    internal class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Word> Words { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<KeyParams> KeyParamss { get; set; }

        public Context()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Hometask3_task1;Trusted_Connection=True;");
        }
    }


    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public List<Cart> Cart { get; set; } = new List<Cart>();
    }

    public class Cart
    {
        public Guid Id { get; set; }

        public User User { get; set; }
        public Product Product { get; set; }
    }

    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double ActionPrice { get; set; }
        public string? DescriptionField1 { get; set; }
        public string? DescriptionField2 { get; set; }
        public string ImageUrl { get; set; }

        public Category Category { get; set; }
        public List<Cart> Carts { get; set; } = new List<Cart>();
        public List<KeyParams> KeyWords { get; set; } = new List<KeyParams>();
    }

    public class Category
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }

        public List<Product> Products { get; set; } = new List<Product>();
    }

    public class KeyParams
    {
        public Guid Id { get; set; }

        public Product Product { get; set; }
        public Word KeyWord { get; set; }
    }

    public class Word
    {
        public Guid Id { get; set; }
        public string Header { get; set; }
        public string Keyword { get; set; }

        public List<KeyParams> ProductLink { get; set; } = new List<KeyParams>();
    }

}
