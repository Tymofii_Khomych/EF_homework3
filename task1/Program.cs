﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics.Contracts;

namespace task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //using (Context db = new Context())
            //{
            //    User user1 = new User { Id = Guid.NewGuid(), Name = "User 1", Login = "user1", Password = "pass1", Email = "user1@example.com" };
            //    User user2 = new User { Id = Guid.NewGuid(), Name = "User 2", Login = "user2", Password = "pass2", Email = "user2@example.com" };

            //    Category categ1 = new Category { Id = Guid.NewGuid(), Name = "Category 1", Icon = "icon1" };
            //    Category categ2 = new Category { Id = Guid.NewGuid(), Name = "Category 2", Icon = "icon2" };
            //    Category categ3 = new Category { Id = Guid.NewGuid(), Name = "Category 3", Icon = "icon3" };

            //    Product product1 = new Product { Id = Guid.NewGuid(), Name = "Product 1", Price = 10.0, ActionPrice = 8.0, DescriptionField1 = "Desc 1", ImageUrl = "url1", Category = categ1 };
            //    Product product2 = new Product { Id = Guid.NewGuid(), Name = "Product 2", Price = 15.0, ActionPrice = 12.0, DescriptionField1 = "Desc 2", ImageUrl = "url2", Category = categ1 };
            //    Product product3 = new Product { Id = Guid.NewGuid(), Name = "Product 3", Price = 20.0, ActionPrice = 18.0, DescriptionField1 = "Desc 3", ImageUrl = "url3", Category = categ2 };
            //    Product product4 = new Product { Id = Guid.NewGuid(), Name = "Product 4", Price = 25.0, ActionPrice = 18.0, DescriptionField1 = "Desc 4", ImageUrl = "url4", Category = categ3 };
            //    Product product5 = new Product { Id = Guid.NewGuid(), Name = "Product 5", Price = 30.0, ActionPrice = 24.0, DescriptionField1 = "Desc 5", ImageUrl = "url5", Category = categ3 };
            //    Product product6 = new Product { Id = Guid.NewGuid(), Name = "Product 6", Price = 35.0, ActionPrice = 30.0, DescriptionField1 = "Desc 6", ImageUrl = "url6", Category = categ2 };
            //    Product product7 = new Product { Id = Guid.NewGuid(), Name = "Product 7", Price = 40.0, ActionPrice = 36.0, DescriptionField1 = "Desc 7", ImageUrl = "url7", Category = categ1 };

            //    categ1.Products.Add(product1);
            //    categ1.Products.Add(product2);
            //    categ1.Products.Add(product7);
            //    categ2.Products.Add(product3);
            //    categ2.Products.Add(product6);
            //    categ3.Products.Add(product4);
            //    categ3.Products.Add(product5);

            //    Word word1 = new Word { Id = Guid.NewGuid(), Header = "Word 1", Keyword = "keyword for product 1" };
            //    Word word2 = new Word { Id = Guid.NewGuid(), Header = "Word 2", Keyword = "keyword for product 2" };
            //    Word word3 = new Word { Id = Guid.NewGuid(), Header = "Word 3", Keyword = "keyword for product 3" };
            //    Word word4 = new Word { Id = Guid.NewGuid(), Header = "Word 4", Keyword = "keyword for product 4" };
            //    Word word5 = new Word { Id = Guid.NewGuid(), Header = "Word 5", Keyword = "keyword for product 5" };
            //    Word word6 = new Word { Id = Guid.NewGuid(), Header = "Word 6", Keyword = "keyword for product 6" };
            //    Word word7 = new Word { Id = Guid.NewGuid(), Header = "Word 7", Keyword = "keyword for product 7" };

            //    Cart cart1 = new Cart { Id = Guid.NewGuid(), User = user1, Product = product1 };
            //    Cart cart2 = new Cart { Id = Guid.NewGuid(), User = user2, Product = product2 };
            //    Cart cart3 = new Cart { Id = Guid.NewGuid(), User = user1, Product = product3 };
            //    Cart cart4 = new Cart { Id = Guid.NewGuid(), User = user2, Product = product4 };
            //    Cart cart5 = new Cart { Id = Guid.NewGuid(), User = user1, Product = product5 };
            //    Cart cart6 = new Cart { Id = Guid.NewGuid(), User = user2, Product = product6 };
            //    Cart cart7 = new Cart { Id = Guid.NewGuid(), User = user1, Product = product7 };

            //    product1.Carts.Add(cart1);
            //    product2.Carts.Add(cart2);

            //    KeyParams keyparams1 = new KeyParams { Id = Guid.NewGuid(), Product = product1, KeyWord = word1 };
            //    KeyParams keyparams2 = new KeyParams { Id = Guid.NewGuid(), Product = product2, KeyWord = word2 };
            //    KeyParams keyparams3 = new KeyParams { Id = Guid.NewGuid(), Product = product3, KeyWord = word3 };
            //    KeyParams keyparams4 = new KeyParams { Id = Guid.NewGuid(), Product = product4, KeyWord = word4 };
            //    KeyParams keyparams5 = new KeyParams { Id = Guid.NewGuid(), Product = product5, KeyWord = word5 };
            //    KeyParams keyparams6 = new KeyParams { Id = Guid.NewGuid(), Product = product6, KeyWord = word6 };
            //    KeyParams keyparams7 = new KeyParams { Id = Guid.NewGuid(), Product = product7, KeyWord = word7 };

            //    word1.ProductLink.Add(keyparams1);
            //    word2.ProductLink.Add(keyparams2);
            //    word3.ProductLink.Add(keyparams3);
            //    word4.ProductLink.Add(keyparams4);
            //    word5.ProductLink.Add(keyparams5);
            //    word6.ProductLink.Add(keyparams6);
            //    word7.ProductLink.Add(keyparams7);

            //    product1.KeyWords.Add(keyparams1);
            //    product2.KeyWords.Add(keyparams2);

            //    user1.Cart.Add(cart1);
            //    user2.Cart.Add(cart2);

            //    db.Users.AddRange(user1, user2);
            //    db.Carts.AddRange(cart1, cart2, cart3, cart4, cart5, cart6, cart7);
            //    db.Products.AddRange(product1, product2, product3, product4, product5, product6, product7);
            //    db.Categories.AddRange(categ1, categ2, categ3);
            //    db.KeyParamss.AddRange(keyparams1, keyparams2, keyparams3, keyparams4, keyparams5, keyparams6, keyparams7);
            //    db.Words.AddRange(word1, word2, word3, word4, word5, word6, word7);
            //    db.SaveChanges();
            //}

            using (Context db = new Context())
            {
                var allUsers = db.Users.
                    Include(u => u.Cart).
                    ThenInclude(c => c.Product).
                    ThenInclude(p => p.Category).ToList();

                if (allUsers.Any())
                {
                    foreach (var user in allUsers)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine($"User: {user.Name}");
                        Console.ResetColor();

                        foreach (var cart in user.Cart)
                        {
                            Console.Write($"--{cart.Product.Name}");
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($" ({cart.Product.Category.Name})");
                            Console.ResetColor();
                        }
                        Console.WriteLine();
                    }
                }
            }

            using (Context db = new Context())
            {
                var allCategories = db.Categories.
                    Include(c => c.Products).
                    ThenInclude(p => p.KeyWords).
                    ThenInclude(kw => kw.KeyWord).
                    ToList();

                if (allCategories.Any())
                {
                    foreach (var category in allCategories)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Category: {category.Name}");
                        Console.ResetColor();

                        foreach (var product in category.Products)
                        {
                            Console.Write($"--{product.Name}");

                            foreach (var kw in product.KeyWords)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine($" ({kw.KeyWord.Keyword})");
                                Console.ResetColor();
                            }
                        }
                        Console.WriteLine();
                    }

                }
            }
        }
    }
}